import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('reusable-test/block_Login with valid credential'), [('username') : 'John Doe', ('pass') : 'ThisIsNotAPassword'], 
    FailureHandling.STOP_ON_FAILURE)

// SELECT FACILITY

switch(select_facility = facility) {
	
	case select_facility = "Tokyo CURA Healthcare Center":
	println(select_facility)
	WebUI.selectOptionByValue(findTestObject('Object Spy Manual/Appointment/dropdown_Facility'), select_facility, false)
	break
	
	case select_facility = "Hongkong CURA Healthcare Center":
	println(select_facility)
	WebUI.selectOptionByValue(findTestObject('Object Spy Manual/Appointment/dropdown_Facility'), select_facility, false)
	break
	
	case select_facility = "Seoul CURA Healthcare Center":
	println(select_facility)
	WebUI.selectOptionByValue(findTestObject('Object Spy Manual/Appointment/dropdown_Facility'), select_facility, false)
	break
		
	default:
	WebUI.selectOptionByValue(findTestObject('Object Spy Manual/Appointment/dropdown_Facility'), select_facility, false)
	break
}

// HOSPITAL READMISSION CHECKBOX

if (hospital_readmission == true ) {
	WebUI.check(findTestObject('Object Spy Manual/Appointment/chk_hospitalReadmission'))
}

// SELECT PROGRAM

switch(select_program = program) {
	
	case select_program = "Medicare":
	println(select_program)
	WebUI.click(findTestObject('Object Spy Manual/Appointment/inpt_programMedicare'))
	break
	
	case select_program = "Medicaid":
	println(select_program)
	WebUI.click(findTestObject('Object Spy Manual/Appointment/inpt_programMedicaid'))
	break
	
	case select_program = "None":
	println(select_program)
	WebUI.click(findTestObject('Object Spy Manual/Appointment/inpt_programNone'))
	break
		
	default:
	WebUI.click(findTestObject('Object Spy Manual/Appointment/inpt_programNone'))
	break
}

WebUI.setText(findTestObject('Object Spy Manual/Appointment/inpt_visitDate'), visit_date)

WebUI.setText(findTestObject('Object Spy Manual/Appointment/inpt_comment'), comment)

WebUI.click(findTestObject('Object Spy Manual/Appointment/btn_BookAppointment'))

// WAIT TO NEXT SCREEN

WebUI.waitForElementVisible(findTestObject('Object Spy Manual/Appointment_Confirmation/btn_GoToHomepage'), 3)

// VERIFY ALL THE ENTRIES

// VERIFY FACILITY
WebUI.verifyElementText(findTestObject('Object Spy Manual/Appointment_Confirmation/txt_facility'), facility)

// VERIFY HOSPITAL READMISSION
if (hospital_readmission == true ) {
	WebUI.verifyElementText(findTestObject('Object Spy Manual/Appointment_Confirmation/txt_hospital_readmission'), 'Yes')
	
}else if(hospital_readmission == false) {
	WebUI.verifyElementText(findTestObject('Object Spy Manual/Appointment_Confirmation/txt_hospital_readmission'), 'No')
	
}

// VERIFY PROGRAM
WebUI.verifyElementText(findTestObject('Object Spy Manual/Appointment_Confirmation/txt_program'), program)

//VERIFY VISIT DATE
WebUI.verifyElementText(findTestObject('Object Spy Manual/Appointment_Confirmation/txt_visit_date'), visit_date)

// VERIFY COMMENT
WebUI.verifyElementText(findTestObject('Object Spy Manual/Appointment_Confirmation/txt_comment'), comment)

WebUI.closeBrowser()

