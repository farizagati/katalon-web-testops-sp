<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>chk_hospitalReadmission</name>
   <tag></tag>
   <elementGuidId>be1616ce-3f34-45f0-a379-c61b8e65ef4a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#chk_hospotal_readmission</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='chk_hospotal_readmission']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>fee2b839-4c8c-432e-be8e-8a8ff4ca9b3d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>867bb352-2dde-47b0-91cd-bdc63b7007d7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>chk_hospotal_readmission</value>
      <webElementGuid>8c768bd5-fd35-4d71-bdd3-fc3da01c0fd3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>hospital_readmission</value>
      <webElementGuid>e4a5ee2e-5906-489b-8da6-ca0a133b560f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Yes</value>
      <webElementGuid>fdc81003-9eb3-4e0e-9c3b-82b72d5c094f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;chk_hospotal_readmission&quot;)</value>
      <webElementGuid>c7ecba65-c08c-4aba-bb0f-a1d422398ed1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='chk_hospotal_readmission']</value>
      <webElementGuid>86ed1e1b-8bd4-4692-8c34-cac14e19f700</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='appointment']/div/div/form/div[2]/div/label/input</value>
      <webElementGuid>c524f5e2-e67d-4269-a09f-6da70f187183</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input</value>
      <webElementGuid>feaa1198-c66b-41e2-a6f2-653001f4aa30</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox' and @id = 'chk_hospotal_readmission' and @name = 'hospital_readmission']</value>
      <webElementGuid>4e20f99b-c3fd-4bf1-a8f0-48ec7c0df2a2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
