<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>txt_hospital_readmission</name>
   <tag></tag>
   <elementGuidId>d80e2f3f-8377-4e00-a315-4eebf875f117</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#hospital_readmission</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//p[@id='hospital_readmission']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>391d464b-7a2c-4a67-a3ea-0ca75dc9fc41</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>hospital_readmission</value>
      <webElementGuid>72a1b07d-6a52-45a3-8ea0-0b835544fe97</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Yes</value>
      <webElementGuid>6063368f-8c3a-4448-a9ca-7b542bb44524</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;hospital_readmission&quot;)</value>
      <webElementGuid>8ba5c991-1bf7-452d-b811-a36d7777421f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//p[@id='hospital_readmission']</value>
      <webElementGuid>ba5142c9-3149-4558-9c40-e9c84ee03bdd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='summary']/div/div/div[3]/div[2]/p</value>
      <webElementGuid>e01495fa-3dbb-4137-b571-e3eb5ecc96f5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Apply for hospital readmission'])[1]/following::p[1]</value>
      <webElementGuid>05417c43-3527-402d-b34e-8f72eae1ee7b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Facility'])[1]/following::p[2]</value>
      <webElementGuid>491bb96d-4456-4051-8755-c49551aa1876</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Healthcare Program'])[1]/preceding::p[1]</value>
      <webElementGuid>c5991d7f-c9a2-425a-8e02-ee8147c4bc90</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Visit Date'])[1]/preceding::p[2]</value>
      <webElementGuid>ff06f626-3cff-4f01-a403-86038e7aa8c2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Yes']/parent::*</value>
      <webElementGuid>a6c7380e-70e2-45cc-8189-71d25c844e7d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div[2]/p</value>
      <webElementGuid>dec5f41a-9803-4974-9d4b-dd596b970e89</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[@id = 'hospital_readmission' and (text() = 'Yes' or . = 'Yes')]</value>
      <webElementGuid>f234cead-6445-4629-9c8b-729a097073ea</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
