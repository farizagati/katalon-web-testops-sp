<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>txt_visit_date</name>
   <tag></tag>
   <elementGuidId>8532692e-b2aa-42bc-82ce-1a5f34bc6b06</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#visit_date</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//p[@id='visit_date']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>687ea398-b7e3-4655-a4b5-c085adcaac2c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>visit_date</value>
      <webElementGuid>5ca99d05-6f01-4309-b4b4-df36ca9d5a07</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>25/07/2023</value>
      <webElementGuid>57101984-437f-4743-a775-dbb84d9140f6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;visit_date&quot;)</value>
      <webElementGuid>97b37fb5-356b-474f-8946-a5f9b4152026</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//p[@id='visit_date']</value>
      <webElementGuid>fff827d1-d208-4df9-b1b2-3d92b46231da</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='summary']/div/div/div[5]/div[2]/p</value>
      <webElementGuid>8dc4a619-07ad-4fd6-af23-6b7afa38f779</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Visit Date'])[1]/following::p[1]</value>
      <webElementGuid>cbc46a91-966c-4a15-af38-72f0c405c167</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Healthcare Program'])[1]/following::p[2]</value>
      <webElementGuid>40cc5d4f-c5c9-462e-934d-af6e3449936c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Comment'])[1]/preceding::p[1]</value>
      <webElementGuid>e2a4eb61-fd62-44dc-9f66-07ab0e5f9d71</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Go to Homepage'])[1]/preceding::p[2]</value>
      <webElementGuid>6c529a3f-1d39-4b0d-8bab-591b00421f41</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='25/07/2023']/parent::*</value>
      <webElementGuid>fa8f4a9b-c245-4617-a8fc-3f4f9e913acf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div[2]/p</value>
      <webElementGuid>3a879c97-2377-49f2-815e-6dd91f3225b5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[@id = 'visit_date' and (text() = '25/07/2023' or . = '25/07/2023')]</value>
      <webElementGuid>aa00f4e0-2f4e-4b9c-a997-b7222025b9a3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
