<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_CURA Healthcare_menu-toggle</name>
   <tag></tag>
   <elementGuidId>b838c5bf-d3a8-4cad-9ff8-a993219cf868</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#menu-toggle</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='menu-toggle']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>b9a2b884-0525-46c9-8aa7-341c949ae531</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>menu-toggle</value>
      <webElementGuid>ef424c6e-b538-4682-8f3b-5ba0232a378c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>#</value>
      <webElementGuid>bfc10e33-dd17-430b-abb9-a0f6e12cb9a9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-dark btn-lg toggle</value>
      <webElementGuid>76c70473-aa4e-450d-bdee-35fb63788256</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;menu-toggle&quot;)</value>
      <webElementGuid>063da3f0-7f01-46ea-97e4-82017835834c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@id='menu-toggle']</value>
      <webElementGuid>bceb9424-e624-41e6-ac66-df2110861e00</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CURA Healthcare'])[1]/preceding::a[2]</value>
      <webElementGuid>8bb432e3-d5ea-4134-aad5-1b760122dfce</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Home'])[1]/preceding::a[3]</value>
      <webElementGuid>1f2e6760-c793-4ef1-96fe-10e703de1b62</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '#')]</value>
      <webElementGuid>5dde15e1-85d8-4f8d-8c54-b1e6f32ace4a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a</value>
      <webElementGuid>0a2a01c0-fb9e-4432-a709-ef60be33b3f6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@id = 'menu-toggle' and @href = '#']</value>
      <webElementGuid>0090ae1e-53b9-4962-a60b-c95b6c9c6803</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
