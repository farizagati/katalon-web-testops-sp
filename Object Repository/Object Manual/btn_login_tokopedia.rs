<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_login_tokopedia</name>
   <tag></tag>
   <elementGuidId>14aef2af-4ba0-47dd-a09c-fc082b08da7a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[@data-testid = 'btnHeaderLogin']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@data-testid='btnHeaderLogin']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-testid</name>
      <type>Main</type>
      <value>btnHeaderLogin</value>
      <webElementGuid>b5ad77d0-d540-46cc-97b1-0d83e7fa91d1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>2db7c52b-c0a3-4d8e-b875-b44178b45e0d</webElementGuid>
   </webElementProperties>
</WebElementEntity>
